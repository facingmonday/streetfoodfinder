const express = require('express');
const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app = express();

app.get('/scrape/:id', function(req, res){
    const data = [];
    if(req.params.id){
        request("https://streetfoodfinder.com/" + req.params.id, (err, response, html)=>{
            const $ = cheerio.load(html);
            if(!err){
                $("script[type='application/ld+json']").each((index, element)=>{
                    if(element.firstChild.data){
                        let obj = JSON.parse(element.firstChild.data);
                        if(obj['@type'] == "FoodEvent"){
                            data.push(obj);
                        }
                    }
                })
            }
            res.json(data);
        })
    } else {
        res.json(data);
    }
})

app.listen('8081')

console.log('Server running on port 8081');

exports = module.exports = app;